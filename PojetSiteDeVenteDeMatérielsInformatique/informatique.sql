-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : lun. 28 déc. 2020 à 16:34
-- Version du serveur :  10.4.11-MariaDB
-- Version de PHP : 7.4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `informatique`
--

-- --------------------------------------------------------

--
-- Structure de la table `accessoire`
--

CREATE TABLE `accessoire` (
  `id_materiel` int(11) NOT NULL,
  `titre` varchar(20) NOT NULL,
  `description` varchar(30) NOT NULL,
  `couleur` varchar(30) NOT NULL,
  `photo` varchar(50) NOT NULL,
  `prix` varchar(30) NOT NULL,
  `stock` varchar(20) NOT NULL,
  `caracteristique` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `accessoire`
--

INSERT INTO `accessoire` (`id_materiel`, `titre`, `description`, `couleur`, `photo`, `prix`, `stock`, `caracteristique`) VALUES
(2, 'carte memoire', 'carte memoire a 16gb', 'noir', 'carte2.jpg', '20,77$', '238', 'carte memoire a une forte capacité de stockage'),
(3, 'Casque', 'Casque avec microphone', 'noir', 'casq1.jpg', '80,99$', '53', 'beau emetteur de son '),
(5, 'clavier', 'clavierAzerti', 'noir', 'cla1.jpg', '18,27$', '426', 'touche tres accessible'),
(6, 'Clavier', 'clavierQuerty', 'noir', 'cla2.jpg', '37,55$', '230', 'touche tactil'),
(7, 'cle', 'Cle usb 32gb', 'noir', 'cle1.jpg', '32,99$', '432', 'capacite de stockage enorme'),
(8, 'cle', 'cle usb 128gb', 'noir', 'cle2.jpg', '45,32$', '125', 'stockage permanant'),
(9, 'souris', 'souris sans fils ', 'rouge', 's1.jpg', '32,27$', '275', 'marche infra-rouge'),
(10, 'souris', 'souris avec fils', 'noir', 's2.jpg', '18,15$', '99', 'beau deplacement '),
(12, 'Webcam', 'claireVisibilite', 'noir', 'web2.jpg', '43,88$', '73', 'capable d\'agrandir et de reduire l,image '),
(13, 'Disque Dur', 'disque dur 500$', 'noir', 'disq1.jpg', '80,88$', '53', 'capable de stocker les donnees de l\'ordinateur'),
(14, 'disque dur', 'disque dur 1000gb', 'noir', 'disq2.jpg', '420,99$', '48', 'stockage permanant des donnees ');

-- --------------------------------------------------------

--
-- Structure de la table `admin`
--

CREATE TABLE `admin` (
  `email` varchar(30) NOT NULL,
  `MotDePasse` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `admin`
--

INSERT INTO `admin` (`email`, `MotDePasse`) VALUES
('maissa@outlook.fr', '1234'),
('mthamadou@gmail.com', '773637479');

-- --------------------------------------------------------

--
-- Structure de la table `commande`
--

CREATE TABLE `commande` (
  `id_commande` int(11) NOT NULL,
  `date_commande` date NOT NULL,
  `etat` varchar(30) NOT NULL,
  `prix` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `commande`
--

INSERT INTO `commande` (`id_commande`, `date_commande`, `etat`, `prix`) VALUES
(0, '0000-00-00', 'en cours', '2875'),
(0, '0000-00-00', 'en cours', '0'),
(0, '0000-00-00', 'en cours', '680'),
(0, '0000-00-00', 'en cours', '1355'),
(0, '0000-00-00', 'en cours', '2555'),
(0, '0000-00-00', 'en cours', '3305'),
(0, '0000-00-00', 'en cours', '3884'),
(0, '0000-00-00', 'en cours', '4434'),
(0, '0000-00-00', 'en cours', '4984'),
(0, '0000-00-00', 'en cours', '4984'),
(0, '0000-00-00', 'en cours', '4984'),
(0, '0000-00-00', 'en cours', '4984'),
(0, '0000-00-00', 'en cours', '3023.35'),
(0, '0000-00-00', 'en cours', '0'),
(0, '0000-00-00', 'en cours', '2390.85'),
(0, '0000-00-00', 'en cours', '2390.85'),
(0, '0000-00-00', 'en cours', '3023.35'),
(0, '0000-00-00', 'en cours', '3023.35'),
(0, '0000-00-00', 'en cours', '3023.35'),
(0, '0000-00-00', 'en cours', '3023.35'),
(0, '0000-00-00', 'en cours', '11451.7'),
(0, '0000-00-00', 'en cours', '11451.7'),
(0, '0000-00-00', 'en cours', '0');

-- --------------------------------------------------------

--
-- Structure de la table `imprimante`
--

CREATE TABLE `imprimante` (
  `id_materiel` int(11) NOT NULL,
  `titre` varchar(20) NOT NULL,
  `description` varchar(50) NOT NULL,
  `couleur` varchar(20) NOT NULL,
  `photo` varchar(30) NOT NULL,
  `prix` varchar(20) NOT NULL,
  `stock` varchar(20) NOT NULL,
  `caracteristique` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `imprimante`
--

INSERT INTO `imprimante` (`id_materiel`, `titre`, `description`, `couleur`, `photo`, `prix`, `stock`, `caracteristique`) VALUES
(1, 'imprimante', 'Imprimante monofonction', 'noir', 'i1.jpg', '430,66$', '275', 'Dinamique'),
(2, 'imprimante', 'Imprimante multifonction', 'noir', 'i2.jpg', '325.99 $', '56', 'Dinamique'),
(3, 'imprimante', 'Imprimante monofonction', 'blanc', 'i3.jpg', '460,66$', '66', 'memoir 250gb Processeur 2.5gh ram 8gb'),
(4, 'imprimante', 'Imprimante multifonction', 'blanc', 'i4.jpg', '1200,99$', '73', 'Un appareil trois en un combinant les fonctions d’'),
(5, 'imprimante', 'Imprimante monofonction', 'noir', 'i5.jpg', '579,99$', '53', 'Une imprimante simple, mais recommandée'),
(6, 'imprimante', 'Imprimante multifonction', 'noir', 'i6.jpg', '680.66 $', '99', 'Dinamique'),
(7, 'imprimante', 'Imprimante monofonction', 'blanc', 'i7.jpg', '675,79$', '53', 'Une imprimante simple, mais recommandée'),
(8, 'imprimante', 'Imprimante multifonction', 'noir', 'i8.jpg', '750,59$', '73', 'Dinamique'),
(9, 'imprimante de bureau', 'imprimante 3D multifonction', 'blanc', 'i13.jpg', '850.99 $', '42', 'imprimante pour les professionnelles de bureau'),
(10, 'imprimante new gener', 'imprimante 3D', 'blanc', 'i17.jpg', '1500 $', '10', 'imprimante pour les professionnelles'),
(11, 'new imprimante', 'la nouvelle imprimante dernière génération', 'noire', 'i16.jpg', '2600 $', '5', 'dédiée aux professionnelles dynamiques');

-- --------------------------------------------------------

--
-- Structure de la table `inscription`
--

CREATE TABLE `inscription` (
  `id_membre` int(11) NOT NULL,
  `nom` varchar(20) NOT NULL,
  `prenom` varchar(20) NOT NULL,
  `email` varchar(20) NOT NULL,
  `DateDeNaissance` date NOT NULL,
  `MotDepass` varchar(20) NOT NULL,
  `telephone` varchar(20) NOT NULL,
  `adresse` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `inscription`
--

INSERT INTO `inscription` (`id_membre`, `nom`, `prenom`, `email`, `DateDeNaissance`, `MotDepass`, `telephone`, `adresse`) VALUES
(2, 'Thiam', 'mouhamadou', 'mthamadou@gmail.com', '2000-12-03', '1234', '438-936-3258', '5122 rue de la peltrie'),
(6, 'test', 'test', 'mthamadou@gmail.com', '2020-12-27', '12', '21', 'karack'),
(7, 'test', 'test', 'mthamadou@gmail.com', '2020-12-27', '12', '21', 'karack'),
(8, 'test', 'test', 'mthamadou@gmail.com', '2020-12-27', '12', '21', 'karack');

-- --------------------------------------------------------

--
-- Structure de la table `laptop`
--

CREATE TABLE `laptop` (
  `id_materiel` int(11) NOT NULL,
  `titre` varchar(20) NOT NULL,
  `description` varchar(30) NOT NULL,
  `couleur` varchar(30) NOT NULL,
  `photo` varchar(40) NOT NULL,
  `prix` varchar(20) NOT NULL,
  `stock` varchar(20) NOT NULL,
  `caracteristique` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `laptop`
--

INSERT INTO `laptop` (`id_materiel`, `titre`, `description`, `couleur`, `photo`, `prix`, `stock`, `caracteristique`) VALUES
(1, 'Mac', 'ordinateurPortable 18pouces', 'bleu', 'l1.jpg', '510,88$', '53', 'memoir 500Gb Processeur 2.5gh ram 8Gb'),
(2, 'Mac', 'ordinateurPortable mac ', 'blanc', 'l2.jpg', '1200$', '80', 'memoir 500Gb Processeur 2.5gh ram 8Gb'),
(3, 'Mac', 'ordinateurPortable', 'blanc', 'l15.jpg', '450,2$', '53', 'memoir 250gb Processeur 2.5gh ram 8gb'),
(4, 'acer', 'ordinateurPortable', 'gris', 'l4.jpg', '770,86$', '45', 'memoir 500Gb Processeur 2.5gh ram 8Gb'),
(5, 'Lenovo', 'ordinateur Portable', 'noir', 'l21.jpg', '850 $', '230', 'memoir 500Gb Processeur 2.5gh ram 8Gb'),
(6, 'Compaq', 'ordinateurPortable', 'gris', 'l22.jpg', '750.99$', '275', 'memoir 250gb Processeur 2.5gh ram 8gb'),
(7, 'Dell', 'ordinateur Portable', 'noir', 'l7.jpg', '765,32$', '89', 'memoir 500Gb Processeur 2.5gh ram 8Gb'),
(8, 'Dell', 'ordinateurPortable', 'noir', 'l8.jpg', '989,99$', '120', 'memoir 250gb Processeur 2.5gh ram 8gb'),
(9, 'Lenovoi', 'ordinateur Portable', 'noir', 'l23.jpg', '1800.95$', '88', 'memoir 500Gb Processeur 2.5gh ram 8Gb'),
(10, 'Hp', 'ordinateurPortable', 'blanc', 'l10.jpg', '1100$,55$', '302', 'memoir 500Gb Processeur 2.5gh ram 8Gb'),
(11, 'asus', 'ordinateur Portable', 'noir', 'l13.jpg', '955,88$$', '89', 'memoir 500Gb Processeur 2.5gh ram 8Gb');

-- --------------------------------------------------------

--
-- Structure de la table `materiels`
--

CREATE TABLE `materiels` (
  `id_meteriel` int(11) NOT NULL,
  `titre` varchar(20) NOT NULL,
  `description` varchar(20) NOT NULL,
  `couleur` varchar(20) NOT NULL,
  `photo` varchar(20) NOT NULL,
  `prix` varchar(20) NOT NULL,
  `stock` varchar(20) NOT NULL,
  `caracteristique` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `materiels`
--

INSERT INTO `materiels` (`id_meteriel`, `titre`, `description`, `couleur`, `photo`, `prix`, `stock`, `caracteristique`) VALUES
(1, 'Dell', 'ordinateurPortable', 'grise', 'p1.jpg', '500$', '53', 'memoir 500Gb Processeur 2.5gh '),
(2, 'Hp', 'ordinateur Bureau', 'blanc', 'p2.jpg', '350$', '80', 'memoir 250gb Processeur 2.5gh '),
(3, 'carte mere', 'AT baby', 'orange', 'p3.jpg', '100$', '80', 'connecteur AGP et 6 connecteur'),
(4, 'carte mere', 'ATX', 'noir', 'p4.jpg', '150$', '230', 'micro-ATX'),
(5, 'Ram', 'SRAM', 'vert', 'p5.jpg', '100$', '120', 'Access Memory'),
(6, 'Ram', 'DRAM', 'grix', 'p6.jpg', '75$', '230', 'Dinamique'),
(7, 'disqueDur', 'disqueDurMecanique', 'orange', 'p7.jpg', '250$', '275', 'capacité de stockage important'),
(8, 'disque dur', 'Le disque dur à mémo', 'grix', 'p8.jpg', '75$', '125', 'Disque dur plus performant'),
(9, 'imprimante', 'Imprimante monofonct', 'blanc', 'p9.jpg', '250$', '80', 'Une imprimante simple, mais re'),
(10, 'imprimante', 'Imprimante multifonc', 'grix', 'p10.jpg', '250$', '73', 'Un appareil trois en un combin'),
(11, 'clavier', 'le clavier à 83 touc', 'blanc', 'p11.jpg', '20$', '275', 'la particularité d\'être dissoc'),
(12, 'souris', 'souris sans fils ', 'noir', 'p12.jpg', '20$', '275', 'capacite de se connecter a dis'),
(13, 'scanner hp', 'Scanner de documents', 'blanc', 'p13.jpg', '750$', '53', 'numérisation rapide de nombreu'),
(14, 'modems dial-up', 'Connexions DSL et câ', 'noir', 'p14.jpg', '85$', '65', 'besoin de terminaison reseau e');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `inscription`
--
ALTER TABLE `inscription`
  ADD PRIMARY KEY (`id_membre`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `inscription`
--
ALTER TABLE `inscription`
  MODIFY `id_membre` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
