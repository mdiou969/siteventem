<?php
session_start();
function creationPanier(){
   if (!isset($_SESSION['panier'])){
      $_SESSION['panier']=array();
      $_SESSION['panier']['id_produit'] = array();
      $_SESSION['panier']['titre'] = array();
      $_SESSION['panier']['qteProduit'] = array();
      $_SESSION['panier']['prixProduit'] = array();
      $_SESSION['panier']['verrou'] = false;
   }
   return true;
}
function ajout($idproduit,$titre,$qte,$prix)
{
	$position=array_search($idproduit, $_SESSION['panier']['id_produit']);
	if($position==false)
		{
			array_push($_SESSION['panier']['id_produit'],$idproduit);
			array_push($_SESSION['panier']['titre'],$titre);
			array_push($_SESSION['panier']['qteProduit'],$qte);
			array_push($_SESSION['panier']['prixProduit'],$prix);
		}

	else
		{
		 $_SESSION['panier']['qteProduit'][$position]+=$qte;

		}
}

function execute_requete($requete)
{ 
$mysqli=new mysqli("localhost","root","","panier");
if($mysqli->connect_error) die (" Erreur de connexion");
$resultat=$mysqli->query($requete);

if(!$resultat)
	{ 
		die("Erreur sur la requete SQL")  ;
     } 

return $resultat;
 }

//contenu du fichier index.php
//  require_once("inc/function.inc.php");

// $req="SELECT * from produit";
// $resultat=execute_requete($req);